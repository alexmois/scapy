import sqlite3
import datetime
import requests
import json
import os

class Scapy:
    
    __cursor = sqlite3.Cursor
    __conn = sqlite3.connect
    __settings = []
    __api_host = ''
    __check_in_ep = ''
    
    
    def __init__(self):
        self.__initialize_db()
        self.__read_sttings()
    
    
    def get_items(self):
        result = self.__cursor.execute("""
                    SELECT * FROM item;
               """)
        
        return result.fetchall()
    
    def __read_sttings(self):
        with open(os.path.dirname(os.path.abspath(__file__)) + '/settings.json') as json_file:
            self.__settings = json.load(json_file)
            self.__api_host = self.__settings['host']
            self.__check_in_ep = self.__settings['check_in']
            
            return self.__settings    
        
        return False
    
    def get_unsend_items(self):
        result = self.__cursor.execute("""
                    SELECT * FROM item WHERE sent = 0;
               """)
        
        return result.fetchall()
    
        
    def put_item(self, id, date_and_time):
        insert_query = ('INSERT INTO item (value, created_at, sent) VALUES ("%s", "%s", 0);' % (id, date_and_time))
        result = self.__cursor.execute(insert_query)
        self.commit()
        
        return result.lastrowid
    
    def send_items(self):
        items = self.get_unsend_items()
        ids = []
        request_body = []
        for rowid, value, created_at, sent, sent_at in items:
            
            ids.append(rowid)      
            request_body.append({
                'card_id':value,
                'created_at':created_at
            })
        
        r = requests.post(self.__api_host + self.__check_in_ep, json=request_body)
        
        if ids and r.status_code == 200:
            self.update_items(ids)
            return True
            
        return r.status_code
    
    def update_items(self, item_ids:list() = None):
        now = datetime.datetime.now()
        sent_at = now.strftime("%Y-%m-%d %H:%M:%S")
        ids = ','.join(str(e) for e in item_ids)
        update_query = ('UPDATE item SET sent = 1 WHERE id IN (%s);' % (ids))
        result = self.__cursor.execute(update_query)
        update_query = ('UPDATE item SET sent_at = "%s" WHERE id IN (%s);' % (sent_at, ids))
        result = self.__cursor.execute(update_query)
        self.commit()
        
        return result        
    
    def __initialize_db(self):
        self.__conn = sqlite3.connect(os.path.dirname(os.path.abspath(__file__)) + "/sca_items.db")
        self.__cursor = self.__conn.cursor()    
        self.__cursor.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='item' ''')

        if self.__cursor.fetchone()[0]==1 :
            print('Table exists.')
        else:
            self.__cursor.execute("""
                    CREATE TABLE item (
                        id INTEGER PRIMARY KEY,
                        value TEXT NOT NULL, 
                        created_at TEXT,
                        sent INTEGER,
                        sent_at TEXT
                    );
               """)
                
        return self.__cursor
  
    def get_db_cursor(self):
        return self.__initialize_db()
    
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.commit()   
        self.__conn.close()     
    
    def commit(self):
        self.__conn.commit()
        
    def get_settings(self):
        self.__read_sttings()
        return self.__settings
    
    def get_api_host(self):
        return self.__api_host
    
    def get_check_in(self):
        return self.__check_in_ep
