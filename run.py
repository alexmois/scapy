
from Sca import Scapy
import datetime
from time import sleep
import sys
from mfrc522 import SimpleMFRC522
import RPi.GPIO as GPIO
import time

now = datetime.datetime.now()


def main():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(18, GPIO.OUT)
    GPIO.output(18, True)

    reader = SimpleMFRC522()
    sca = Scapy()

    
    print('Program started')
    print('\n')
    print('Host: ' + sca.get_api_host())
    print('Check In: ' + sca.get_check_in())
    print('\n')
    
    try:
        while True:
            print("Hold a tag near the reader")
            card_id, card_value = reader.read()
            GPIO.output(18, False)
            print("ID: %s\nText: %s" % (card_id, card_value))
            now = datetime.datetime.now()
            sca.put_item(card_id, now.strftime("%Y-%m-%d %H:%M:%S"))
                        
            try:
                code = sca.send_items()
                if code is True:
                    print('Data sent')
                else:
                    raise Exception()
                sleep(1)
            except Exception as e:
                print('Something went wrong. Please, check network available')
                sleep(1)
            
            GPIO.output(18, True)
            
    except KeyboardInterrupt:
        GPIO.cleanup()
        raise
        


if __name__ == "__main__":
    main()
    
    
  